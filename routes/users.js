const express = require("express");
const route = express.Router();
const userController = require('../controllers/users')
const middleware = require("../middleware/auth");

route.get("/", middleware.isAuthenticated, userController.getAll)
route.get("/:id/vote/:user_vote_id", middleware.isAuthenticated, userController.userVote)
route.get("/:id/unvote/:user_unvote_id", middleware.isAuthenticated, userController.userUnvote)

module.exports = route