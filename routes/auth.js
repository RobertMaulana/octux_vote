const express = require("express");
const route = express.Router();
const middleware = require("../middleware/auth");
const authController = require('../controllers/auth')

route.post("/signup", authController.signup)
route.post("/signin", authController.signin);
route.get("/signout", middleware.isAuthenticated, authController.signout);

module.exports = route