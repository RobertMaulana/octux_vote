const request = require("supertest");
const server = require("../server");
const assert = require('chai').assert
const faker = require('faker')
const generatePassword = require('password-generator')

let agent;
beforeAll(async () => {
  agent = request.agent(server.listen());
});

describe("POST /signup", function() {
  test("user not able to signup due duplicate email", async () => {
    await agent
        .post('/signup')
        .set('Accept', 'application/json')
        .send({
          "email": "c@gmail.com",
          "password": "qwerty"
        })
        .expect(500)
        .then(response => {
            assert(response.body.message, 'The email address is already in use by another account.')
        })
  }, 10000);
  test("user able to signup", async () => {
    await agent
        .post('/signup')
        .set('Accept', 'application/json')
        .send({
          "email": faker.internet.email() + generatePassword(),
          "password": generatePassword()
        })
        .expect(201)
        .then(response => {
            assert(response.body.message, 'success')
        })
  }, 10000);
});

describe("POST /signin", function() {
  test("user will be fail to signin wrong email address and password", async () => {
    await agent
        .post('/signin')
        .set('Accept', 'application/json')
        .send({
          "email": faker.internet.email(),
          "password": generatePassword()
        })
        .expect(500)
        .then(response => {
            assert(response.body.message, 'There is no user record corresponding to this identifier. The user may have been deleted.')
        })
  }, 10000);
  test("user success to signin", async () => {
    await agent
        .post('/signin')
        .set('Accept', 'application/json')
        .send({
          "email": "admin@gmail.com",
          "password": "adminganteng"
        })
        .expect(200)
        .then(response => {
            assert(response.body.message, 'success')
        })
  }, 10000);
});
