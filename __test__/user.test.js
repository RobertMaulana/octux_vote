const request = require("supertest");
const server = require("../server");
const assert = require("chai").assert;
const faker = require("faker");
const generatePassword = require("password-generator");
const Async = require("async");

let agent;
beforeAll(async () => {
  agent = request.agent(server.listen());
});

let userId;
let userVoteId;
Async.auto(
  {
    user1_signup: callback => {
      request
        .agent(server.listen())
        .post("/signup")
        .set("Accept", "application/json")
        .send({
          email: faker.internet.email(),
          password: generatePassword()
        })
        .end((err, response) => callback(err, response.body));
    },
    user2_signup: callback => {
      request
        .agent(server.listen())
        .post("/signup")
        .set("Accept", "application/json")
        .send({
          email: faker.internet.email(),
          password: generatePassword()
        })
        .end((err, response) => callback(err, response.body));
    }
  },
  (err, results) => {
    if (err) console.error(err);
    userId = results["user1_signup"]["userId"];
    userVoteId = results["user2_signup"]["userId"];
  }
);

describe("GET /users", function() {
  test("will be fail get all users due credentials", async () => {
    await agent
      .get("/users")
      .set("Accept", "application/json")
      .expect(403)
      .then(response => {
        assert(response.body.message, "forbidden error");
      });
  }, 10000);
  test("signin to perform call get all user api", async () => {
    await agent
      .post("/signin")
      .set("Accept", "application/json")
      .send({
        email: "admin@gmail.com",
        password: "adminganteng"
      })
      .expect(200)
      .then(response => {
        assert(response.body.message, "success");
      });
  }, 15000);
  test("success get all users", async () => {
    await agent
      .get("/users")
      .set("Accept", "application/json")
      .expect(200)
      .then(response => {
        expect(Array.isArray([response.body])).toBe(true);
      });
  });
});

describe("GET /vote", function() {
  test("will be success to vote other user", async () => {
    await agent
      .get(`/users/${userId}/vote/${userVoteId}`)
      .set("Accept", "application/json")
      .then(response => {
        expect(typeof response).toBe("object");
      });
  });
});

describe("GET /unvote", function() {
  test("will be success to unvote other user", async () => {
    await agent
      .get(`/users/${userId}/unvote/${userVoteId}`)
      .set("Accept", "application/json")
      .then(response => {
        expect(typeof response).toBe("object");
      });
  }, 25000);
});
