const firebase = require("firebase");

module.exports = {
  isAuthenticated: function(req, res, next) {
    const user = firebase.auth().currentUser;
    if (user !== null) {
      next();
    } else {
      res
        .status(403)
        .send({
          message: "forbidden error"
        })
        .end();
    }
  }
};
