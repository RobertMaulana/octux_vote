# Octux vote
Simple application vote

## Development
NPM is okay but preferable to use Yarn.
Please make sure you have read this [wiki page](https://github.com/pacifictoy/assetfolio-jp/wiki/contribute) to see how to contribute

# Clone repository

```
$ git clone https://gitlab.com/RobertMaulana/octux_vote.git
```

# Frontend
### Requirements
- [jest](https://facebook.github.io/jest/)
- [reactjs](https://reactjs.org/)
- [axios](https://github.com/axios/axios)
- [styled components](https://www.styled-components.com/)
- [history](https://github.com/browserstate/history.js/)

## Setup dependencies
Frontend need some dependencies so we need to install before run it.
```
$ cd octux_vote/client
$ npm install
```

### Frontend env
`.env` contains the credentials, eg: host, etc.`.env` definitions are in the root directory of `client`.
```
$ cat .env
REACT_APP_ENV=http://localhost:8000
$
```

### Run App
To run frontend in development environment use this command
```
$ npm start
```
Frontend will run through `http://localhost:3000`

# Backend

### Requirements
- [nodejs](https://nodejs.org/en/)
- [axios](https://github.com/axios/axios)
- [express](https://expressjs.com/)
- [sequelize](http://docs.sequelizejs.com/)
- [Mysql](https://www.mysql.com/)
- [Dotenv](https://www.npmjs.com/package/dotenv)
- [Cord](https://www.npmjs.com/package/cors)
- [Firebase](https://firebase.google.com/?hl=id)
- [Body parser](https://www.npmjs.com/package/body-parser)


## Setup dependencies
Backend need some dependencies so we need to install before run it.
```
$ cd octux_vote
$ npm install
```

### Server env
`.env` contains the credentials, eg: database, etc.`.env` definitions are in the root directory of `server`.
```
$ cat .env
DB_NAME={db_name}
DB_USERNAME={db_user}
DB_PASS={db_pass}
DB_HOST={db_host}
DB_PORT={db_port}
DB_DIALECT={dialect}
$

```

### Run Server
To run server in development environment use this command
```
$ node server.js
```
Backend will run through `http://localhost:8000`

## Api documentation
#### 1. Signup
User signup api
#### Example request:
Endpoint: `localhost:8000/signup`
Method: `POST`
```
{
	"email": "test@mail.com",
	"password": "#$%^&*"
}
```
#### Success response:
```
{
	"message": "success"
}
```
#### Fail response:
```
{
    "message": "The email address is already in use by another account."
}
```

#### 2. Signin
User signin api
#### Example request:
Endpoint: `localhost:8000/signin`
Method: `POST`
```
{
	"email": "test@mail.com",
	"password": "#$%^&*"
}
```
#### Success response:
```
{
    "message": "success",
    "uid": "s3acbkHp4wQMSxxxxxxxxxx",
    "email": "test@mail.com",
    "userId": 7
}
```
#### Fail response:
```
{
    "message": "There is no user record corresponding to this identifier. The user may have been deleted."
}
```
#### 3. Signout
User signout api
#### Example request:
Endpoint: `localhost:8000/signout`
Method: `GET`
#### Success response:
```
{
    "message": "success",
    "desc": "signout"
}
```
#### 4. GetUsers
get users api
#### Example request:
Endpoint: `localhost:8000/users`
Method: `GET`
#### Success response:
```
[
    {
        "id": 9,
        "email": "test@gmail.com",
        "users_votes": [
            {
                "user_id": 8,
                "user_vote_id": 9
            }
        ]
    },
    {
        "id": 10,
        "email": "d@gmail.com",
        "users_votes": []
    },
    {...},
    {...}
]
```
#### 5. Vote
vote to other user api
#### Example request:
Endpoint: `localhost:8000/users/{:user_id}/vote/{:user_vote_id}`
Method: `GET`
#### Success response:
```
{
    "id": 44,
    "user_id": "7",
    "user_vote_id": "9",
    "updatedAt": "2019-07-05T06:53:58.532Z",
    "createdAt": "2019-07-05T06:53:58.532Z"
}
```
#### Fail response:
```
{
    "message": "already voted to this user"
}
```
```
{
    "message": "user id not found"
}
```
```
{
    "message": "user vote id not found"
}
```
#### 5. Unvote
vote to other user api
#### Example request:
Endpoint: `localhost:8000/users/{:user_id}/unvote/{:user_vote_id}`
Method: `GET`
#### Success response:
```
{
    "message": "unvoted"
}
```
#### Fail response:
```
{
    "message": "no data vote found"
}
```
```
{
    "message": "user id not found"
}
```
```
{
    "message": "user vote id not found"
}
```

## Running test without coverage table
```
$ npm run test
```

#### Example output
```
 PASS  __test__/auth.test.js (12.92s)
 PASS  __test__/user.test.js (17.617s)
```

## Running test with coverage table
```
$ npm run test:coverage
```

#### Example output
```
 PASS  __test__/user.test.js (11.095s)
 PASS  __test__/auth.test.js (11.91s)
-------------------------|----------|----------|----------|----------|-------------------|
File                     |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
-------------------------|----------|----------|----------|----------|-------------------|
All files                |    75.82 |    31.82 |       75 |    75.82 |                   |
 hyperladger             |    89.47 |       25 |        0 |    89.47 |                   |
  db.js                  |      100 |      100 |      100 |      100 |                   |
  server.js              |    86.67 |       25 |        0 |    86.67 |             17,18 |
 hyperladger/config      |      100 |      100 |      100 |      100 |                   |
  config.js              |      100 |      100 |      100 |      100 |                   |
 hyperladger/controllers |    63.16 |    28.95 |    83.33 |    63.16 |                   |
  auth.js                |    65.79 |    33.33 |    66.67 |    65.79 |... 63,64,65,66,68 |
  users.js               |     61.4 |    26.92 |      100 |     61.4 |... 00,101,103,108 |
 hyperladger/firebase    |      100 |      100 |      100 |      100 |                   |
  index.js               |      100 |      100 |      100 |      100 |                   |
 hyperladger/middleware  |      100 |      100 |      100 |      100 |                   |
  auth.js                |      100 |      100 |      100 |      100 |                   |
 hyperladger/models      |      100 |      100 |      100 |      100 |                   |
  users.js               |      100 |      100 |      100 |      100 |                   |
  users_vote.js          |      100 |      100 |      100 |      100 |                   |
 hyperladger/routes      |      100 |      100 |      100 |      100 |                   |
  auth.js                |      100 |      100 |      100 |      100 |                   |
  users.js               |      100 |      100 |      100 |      100 |                   |
-------------------------|----------|----------|----------|----------|-------------------|

Test Suites: 2 passed, 2 total
Tests:       9 passed, 9 total
Snapshots:   0 total
Time:        13.03s
```
