import React from 'react'
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
import RestrictedApp from './containers/restricted/home'
import Signup from './containers/public/signup'
import Signin from './containers/public/signin'
import { createBrowserHistory } from 'history'
const history = createBrowserHistory()

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (
        isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/signin',
              state: { from: props.location }
            }}
          />
        )
      )}
    />
  )
}

export default class PublicRoutes extends React.PureComponent {
  state = {
    isLoggedIn : localStorage.getItem('uid') !== null ? true : false
  }
  signInTrigger = () => {
    this.setState({isLoggedIn: true})
  }
  render() {
    return (
      <Router>
        <Switch>
          <Route
            exact
            path={'/signup'}
            render={() => <Signup history={history} />}
          />
          <Route
            exact
            path={'/signin'}
            render={() => <Signin history={history} signInTrigger={this.signInTrigger}/>}
          />
          <RestrictedRoute
            path='/'
            component={RestrictedApp}
            isLoggedIn={this.state.isLoggedIn}
          />
        </Switch>
      </Router>
    )
  }
}
