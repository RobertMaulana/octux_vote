export default {
    signin: `${process.env.REACT_APP_ENV}/signin`,
    signup: `${process.env.REACT_APP_ENV}/signup`,
    signout: `${process.env.REACT_APP_ENV}/signout`,
    getAllUsers: `${process.env.REACT_APP_ENV}/users`,
    vote: `${process.env.REACT_APP_ENV}/users`,
    unvote: `${process.env.REACT_APP_ENV}/users`
}