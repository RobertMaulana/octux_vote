import React from "react";
import { FlashStyle } from "./flash.style";

export default props => {
  return (
    <FlashStyle {...props}>
      <div className="snackbar">{props.message}</div>
    </FlashStyle>
  );
};
