import React from "react";
import {NavbarStyle} from './navbar.style'
import axios from "axios";
import api from "../../helpers/api";
const currentUser = localStorage.getItem('email')

export default class Navbar extends React.PureComponent {
  state = {
    onSignout: false
  }
  signout = async () => {
    this.setState({onSignout: true})
    let response
    try {
      localStorage.removeItem('email')
      localStorage.removeItem('uid')
      localStorage.removeItem('userId')
      response = await axios.get(api.signout)
      setTimeout(() => {
        window.location.reload()
      }, 500)
    } catch (error) {
      console.error(error)
    }
  }
  render () {
    return (
      <NavbarStyle>
        <div className="topnav">
          <a href="javascript:void(0)" style={{float: 'left'}}>
            Welcome, {currentUser}
          </a>
          <a href="javascript:void(0)" onClick={this.signout}>
            {
              this.state.onSignout ? <i className="fa fa-spinner fa-spin" /> : ''
            }
            &nbsp; Sign out
          </a>
        </div>
      </NavbarStyle>
    );
  }
};
