import React from "react";
import { CardStyle } from "./card.style";
import axios from "axios";
import api from "../../helpers/api";
import FlashMessage from "../../components/FlashMessage";

export default class Card extends React.PureComponent {
  state = {
    errorMessage: "",
    showFlash: false,
    isClicked: false
  };
  vote = async () => {
    this.setState({isClicked: true})
    let response;
    const { id } = this.props;
    const userId = localStorage.getItem("userId");
    try {
      response = await axios.get(`${api.vote}/${userId}/vote/${id}`);
      if (response.status === 200) {
        this.setState({isClicked: false})
        this.props.refetch()
      }else {
        this.setState({ errorMessage: "unable to vote", isClicked: false, showFlash: true });
      }
    } catch (error) {
      if (error.response.status === 409) {
        this.setState({
          errorMessage: error.response.data.message,
          showFlash: true,
          isClicked: false
        });
        setTimeout(() => {
          this.setState({ errorMessage: "", showFlash: false });
        }, 2000);
      } else {
        this.setState({
          errorMessage: "unable to vote",
          showFlash: true,
          isClicked: false
        });
      }
    }
  };
  unvote = async () => {
    this.setState({isClicked: true})
    let response;
    const { id } = this.props;
    const userId = localStorage.getItem("userId");
    try {
      response = await axios.get(`${api.unvote}/${userId}/unvote/${id}`);
      if (response.status === 200) {
        this.setState({isClicked: false})
        this.props.refetch()
      }else {
        this.setState({ errorMessage: "unable to unvote", isClicked: false, showFlash: true });
      }
    } catch (error) {
      if (error.response.status === 409) {
        this.setState({
          errorMessage: error.response.data.message,
          showFlash: true,
          isClicked: false
        });
        setTimeout(() => {
          this.setState({ errorMessage: "", showFlash: false });
        }, 2000);
      } else {
        this.setState({
          errorMessage: "unable to unvote",
          showFlash: true,
          isClicked: false
        });
      }
    }
  };
  render() {
    const { email, isVote } = this.props;
    return (
      <CardStyle>
        <FlashMessage
          isOpen={this.state.showFlash}
          message={this.state.errorMessage}
          type="error"
        />
        <div className="card">
          <img
            src="https://www.w3schools.com/howto/img_avatar.png"
            alt="avatar"
            style={{ width: "100%" }}
          />
          <h1>{email}</h1>
          <p>
            {isVote ? (
              <button onClick={this.unvote} className='unvote' disabled={this.state.isClicked ? true : false}>
                {
                  this.state.isClicked ? <i className="fa fa-spinner fa-spin" /> : ''
                }
                &nbsp; Unvote
              </button>
            ) : (
              <button onClick={this.vote} disabled={this.state.isClicked ? true : false}>
                {
                  this.state.isClicked ? <i className="fa fa-spinner fa-spin" /> : ''
                }
                &nbsp; Vote
              </button>
            )}
          </p>
        </div>
      </CardStyle>
    );
  }
}
