import styled from "styled-components";

export const CardStyle = styled.div`
  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    max-width: 300px;
    text-align: center;
    font-family: arial;
    margin: 10px;
  }

  .card h1 {
    word-wrap: break-word;
  }

  .card button {
    border: none;
    outline: 0;
    padding: 12px;
    color: white;
    background-color: green;
    text-align: center;
    cursor: pointer;
    width: 100%;
    font-size: 18px;
  }
  .card .unvote {
    background-color: red;
  }

  .card button:hover {
    opacity: 0.7;
  }
`;
