import React from "react";
import { LoaderStyle } from "./loader.style";

export default () => {
  return (
    <LoaderStyle>
      <div className="loader" />
    </LoaderStyle>
  );
};
