import React from "react";
import { SigninContainer } from "./signin.style";
import { Redirect } from "react-router-dom";
import FlashMessage from "../../components/FlashMessage";
import axios from "axios";
import api from "../../helpers/api";

export default class Signin extends React.PureComponent {
  state = {
    showFlashMessage: false,
    signinStatus: "",
    uid:
      localStorage.getItem("uid") !== null ? localStorage.getItem("uid") : null,
    isSubmit: false
  };
  signup = () => {
    window.location.href = '/signup'
  }
  onSubmit = async event => {
    event.preventDefault();
    this.setState({ isSubmit: true });
    const form = event.target;
    const data = new FormData(form);
    let response;
    try {
      response = await axios.post(api.signin, {
        email: data.get("email"),
        password: data.get("password")
      });
      if (response.status === 200) {
        localStorage.setItem("uid", response.data.uid);
        localStorage.setItem("email", response.data.email);
        localStorage.setItem("userId", response.data.userId);
        window.location.href = "/";
        this.setState({
          uid: response.data.uid,
          isSubmit: false
        });
        this.props.signInTrigger();
      } else {
        this.setState({
          signinStatus: "signin failed",
          showFlashMessage: true,
          isSubmit: false
        });
      }
      this.setState({
        uid: response.data.uid
      });
    } catch (error) {
      this.setState({
        signinStatus: error.response.data.message,
        showFlashMessage: true,
        isSubmit: false
      });
      setTimeout(() => {
        this.setState({signinStatus: '', showFlashMessage: false, isSubmit: false})
      }, 2000)
    }
  };
  render() {
    if (this.state.uid !== null) {
      return <Redirect to={"/"} />;
    }
    return (
      <SigninContainer>
        <FlashMessage
          isOpen={this.state.showFlashMessage}
          message={this.state.signinStatus}
          type="error"
        />
        <form onSubmit={this.onSubmit} style={{ border: "1px solid #ccc" }}>
          <div className="container">
            <h1>Sign In</h1>
            <hr />
            <label htmlFor="email">
              <b>Email</b>
            </label>
            <input
              type="email"
              placeholder="Your email address"
              name="email"
              required
            />
            <label htmlFor="psw">
              <b>Password</b>
            </label>
            <input
              type="password"
              placeholder="Your new password"
              name="password"
              required
            />
            <p>
              Don't have an account yet?{" "}
              <span
                style={{ color: "dodgerblue", cursor: "pointer" }}
                onClick={this.signup}
              >
                Signup
              </span>
              .
            </p>
            <div className="clearfix">
              <button
                type="submit"
                className="signupbtn"
                disabled={this.state.isSubmit ? true : false}
              >
                {this.state.isSubmit ? (
                  <i className="fa fa-spinner fa-spin" />
                ) : (
                  ""
                )}
                &nbsp;Sign In
              </button>
            </div>
          </div>
        </form>
      </SigninContainer>
    );
  }
}
