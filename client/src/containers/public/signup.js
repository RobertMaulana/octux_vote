import React from "react";
import { SignupContainer } from "./signup.style";
import FlashMessage from "../../components/FlashMessage";
import axios from "axios";
import api from "../../helpers/api";

export default class Signup extends React.PureComponent {
  state = {
    showFlashMessage: "",
    signUpStatus: "",
    isSubmit: false,
    type: ''
  };
  signin = () => {
    window.location.href = "/signin"
  }
  onSubmit = async event => {
    event.preventDefault();
    this.setState({ isSubmit: true });
    const form = event.target;
    const data = new FormData(form);
    let response;
    try {
      response = await axios.post(api.signup, {
        email: data.get("email"),
        password: data.get("password")
      });
      if (response.status === 201) {
        this.setState({
            signUpStatus: "signup was success. You will be redirect to signin page",
            showFlashMessage: true,
            isSubmit: false,
            type: 'success'
          });
          setTimeout(() => {
            this.setState({
                signUpStatus: "",
                showFlashMessage: false,
                isSubmit: false,
                type: ''
              });
            window.location.href = "/signin";
          }, 3000)
      } else {
        this.setState({
          signUpStatus: "signin failed",
          showFlashMessage: true,
          isSubmit: false,
          type: 'error'
        });
        setTimeout(() => {
            this.setState({signUpStatus: '', showFlashMessage: false, isSubmit: false, type: ''})
          }, 2000)
      }
    } catch (error) {
      this.setState({
        signUpStatus: error.response.data.message,
        showFlashMessage: true,
        isSubmit: false,
        type: 'error'
      });
      setTimeout(() => {
        this.setState({signUpStatus: '', showFlashMessage: false, isSubmit: false, type: ''})
      }, 2000)
    }
  };
  render() {
    return (
      <SignupContainer>
        <FlashMessage
          isOpen={this.state.showFlashMessage}
          message={this.state.signUpStatus}
          type={this.state.type}
        />
        <form onSubmit={this.onSubmit} style={{ border: "1px solid #ccc" }}>
          <div className="container">
            <h1>Sign Up</h1>
            <p>Please fill in this form to create an account.</p>
            <hr />
            <label htmlFor="email">
              <b>Email</b>
            </label>
            <input
              type="email"
              placeholder="Your email address"
              name="email"
              required
            />
            <label htmlFor="password">
              <b>Password</b>
            </label>
            <input
              type="password"
              placeholder="Your new password"
              name="password"
              required
            />
            <p>
              Already have an account?{" "}
              <span
                style={{ color: "dodgerblue", cursor: "pointer" }}
                onClick={this.signin}
              >
                Signin
              </span>
              .
            </p>
            <div className="clearfix">
              <button
                type="submit"
                className="signupbtn"
                disabled={this.state.isSubmit ? true : false}
              >
                {this.state.isSubmit ? (
                  <i className="fa fa-spinner fa-spin" />
                ) : (
                  ""
                )}
                &nbsp; Sign Up
              </button>
            </div>
          </div>
        </form>
      </SignupContainer>
    );
  }
}
