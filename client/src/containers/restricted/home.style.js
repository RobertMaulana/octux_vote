import styled from "styled-components";

export const HomeStyle = styled.div`
  .home-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
`;
