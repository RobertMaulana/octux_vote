import React from "react";
import Navbar from "../../components/Navbar";
import Card from "../../components/Card";
import Loader from "../../components/Loader";
import axios from "axios";
import api from "../../helpers/api";
import { HomeStyle } from "./home.style";

export default class Home extends React.PureComponent {
  state = {
    data: [],
    errorMessage: "",
    userId: localStorage.getItem("userId")
  };
  fetchUsers = async () => {
    let response;
    try {
      response = await axios.get(api.getAllUsers);
      if (response.status === 200) {
        this.setState({ data: response.data });
      } else {
        this.setState({ errorMessage: "unable to fetch" });
      }
    } catch (error) {
      console.error(error);
      this.setState({ errorMessage: "unable to fetch" });
    }
  }
  componentDidMount() {
    this.fetchUsers()
  }
  refetch = () => {
    this.fetchUsers()
  }
  renderCard = users => {
    return users.map((user, index) => {
      let isVote = false;
      if (user.users_votes.length > 0) {
        user.users_votes.forEach(element => {
          if (element.user_id === Number(this.state.userId)) {
            isVote = true;
          }
        });
      }
      return (
        <Card 
            email={user.email} 
            id={user.id} 
            isVote={isVote} 
            key={index}
            refetch={this.refetch}
        />
      );
    });
  };
  render() {
    return (
      <HomeStyle>
        <Navbar />
        <div className="home-container">
          {this.state.data.length < 1 ? (
            <Loader />
          ) : (
            this.renderCard(this.state.data)
          )}
        </div>
      </HomeStyle>
    );
  }
}
