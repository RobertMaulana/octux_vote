const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors')

// route
const authRoute = require('./routes/auth')
const userRoute = require('./routes/users')

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', authRoute)
app.use('/users', userRoute)

if (process.env.NODE_ENV !== 'test') {
  app.listen(process.env.port || 8000, () => {
    console.info("server started ...");
  });
}

module.exports = app
