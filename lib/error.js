const ERRORS = {
    Common: {
      code: 400,
      internal: '400000',
      message: 'General Error'
    },
    MissingParameters: {
      code: 400,
      internal: '400001',
      message: 'Insufficient Parameters'
    },
    MissingEmail: {
      code: 400,
      internal: '400002',
      message: 'Email is Required'
    },
    MissingPassword: {
      code: 400,
      internal: '400003',
      message: 'Password is Required'
    },
    DuplicateEmailOrName: {
      code: 400,
      internal: '400005',
      message: 'Duplicate Email or Name'
    },
    DuplicateEmail: {
      code: 400,
      internal: '400007',
      message: 'Duplicate Email'
    },
    InvalidPassword: {
      code: 401,
      internal: '401001',
      message: 'Invalid Password'
    },
    // 404
    NotFound: {
      code: 404,
      internal: '404000',
      message: 'Not Found'
    }
  }
  
  class HttpError extends Error {
    constructor ({message = ERRORS.Common.message, code, internal}) {
      super(message)
      this.code = code || ERRORS.Common.code
      this.internal = internal || ERRORS.Common.internal
    }
  }
  
  module.exports = {HttpError, ERRORS}
  