const Users = require("../models/users");
const Vote = require("../models/users_vote");
const Sequelize = require("sequelize");
const $ = Sequelize.Op;
const storage = require("node-persist");

const getAll = async (req, res) => {
  let response;
  try {
    response = await Users.findAll({
      where: { email: { [$.not]: await storage.getItem("email") } },
      order: [["id", "ASC"]],
      attributes: ["id", "email"],
      include: [
        {
          model: Vote,
          attributes: ["user_id", "user_vote_id"]
        }
      ]
    });
  } catch (error) {
    console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
  res.status(200).json(response);
};

const userVote = async (req, res) => {
  const user_id = req.params.id;
  const user_vote_id = req.params.user_vote_id;
  let response;
  let existingVote;
  let checkUserId
  let checkUserVoteId
  try {
    checkUserId = await Users.findOne({
      where: {id: user_id}
    })
    if (checkUserId === null) {
      return res.status(400).json({ message: "user id not found" });
    }
    checkUserVoteId = await Users.findOne({
      where: {id: user_vote_id}
    })
    if (checkUserVoteId === null) {
      return res.status(400).json({ message: "user vote id not found" });
    }
    existingVote = await Vote.findAll({
      where: { user_id, user_vote_id }
    });
    if (existingVote.length > 0) {
      return res.status(409).json({ message: "already voted to this user" });
    }
    response = await Vote.create({ user_id, user_vote_id });
  } catch (error) {
    console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
  res.status(200).json(response);
};

const userUnvote = async (req, res) => {
  const user_id = req.params.id;
  const user_vote_id = req.params.user_unvote_id;
  let response;
  let checkUserId
  let checkUserVoteId
  let existingVote;
  try {
    checkUserId = await Users.findOne({
      where: {id: user_id}
    })
    if (checkUserId === null) {
      return res.status(400).json({ message: "user id not found" });
    }
    checkUserVoteId = await Users.findOne({
      where: {id: user_vote_id}
    })
    if (checkUserVoteId === null) {
      return res.status(400).json({ message: "user vote id not found" });
    }
    existingVote = await Vote.findAll({
      where: { user_id, user_vote_id }
    });
    if (existingVote.length < 1) {
      return res.status(404).json({ message: "no data vote found" });
    }
    response = await Vote.destroy({ where: { user_id, user_vote_id } });
  } catch (error) {
    console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
  if (response === 1) {
    res.status(200).json({ message: "unvoted" });
  } else {
    res.status(400).json({ message: "failed" });
  }
};

module.exports = {
  getAll,
  userVote,
  userUnvote
};
