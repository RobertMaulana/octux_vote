const firebase = require("../firebase");
const Users = require("../models/users");
const storage = require("node-persist");

const signup = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  let response;
  try {
    await firebase.auth().createUserWithEmailAndPassword(email, password);
    response = await Users.create({ email, password });
  } catch (error) {
    // console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
  return res.status(201).json({
    message: 'success',
    userId: response.id
  });
};

const signin = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  let response;
  let userDetails;
  try {
    response = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
    userDetails = await Users.findOne({ where: { email } });
    await storage.init();
    await storage.setItem("email", response.user.email);
    return res.status(200).json({
      message: "success",
      uid: response.user.uid,
      email: response.user.email,
      userId: userDetails.id
    });
  } catch (error) {
    // console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
};

const signout = async (req, res) => {
  try {
    await storage.removeItem("email");
    await firebase.auth().signOut();
    return res.status(201).json({
      message: "success",
      desc: "signout"
    });
  } catch (error) {
    console.error(error.errors);
    if (error.errors && error.errors.length > 0) {
      const first = error.errors[0];
      return res.status(400).json({ message: first.message });
    }
    return res.status(500).json({ message: error.message });
  }
};

module.exports = {
  signup,
  signin,
  signout
};
