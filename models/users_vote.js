const Sequelize = require('sequelize')
const db = require('../db')
const Users = require('./users')

const Vote = db.define('users_vote', {
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  user_vote_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
}, {
  timestamps: true,
  indexes: [
    {
      fields: ['id']
    }
  ]
})

// Associations
Vote.belongsTo(Users, { foreignKey: 'user_id' })
Users.hasMany(Vote, { foreignKey: 'user_vote_id' })

module.exports = Vote
